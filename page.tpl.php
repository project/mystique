<?php
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head profile="http://gmpg.org/xfn/11">
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>
  
  <!--[if lte IE 6]>
    <style type="text/css" media="screen">
      @import "<?php  print base_path() . path_to_theme() ?>/ie6.css";
    </style>
  <![endif]-->
  <!--[if IE 7]>
    <style type="text/css" media="screen">
      @import "<?php  print base_path() . path_to_theme() ?>/ie7.css";
    </style>
  <![endif]-->
  
</head>
<body class="single-page blog col-2-right <?php print $body_classes; ?>">
 <div id="page">

  <div class="shadow-left page-content header-wrapper">
    <div class="shadow-right">
      <div id="header" class="bubbleTrigger">
         <div id="site-title">
           <h1 id="logo"><a href="<?php print $front_page; ?>"><?php print $site_name; ?></a></h1>  
           <p class="headline"><?php print $site_slogan; ?></p>
         </div>
         <?php if ($p_links): ?>
           <ul id="navigation">
             <?php print $p_links; ?>
           </ul>
         <?php endif ?>        
      </div>
    </div>
  </div>

  <!-- left+right bottom shadow -->
  <div class="shadow-left page-content main-wrapper">
    <div class="shadow-right"> 
      <!-- main content: primary + sidebar(s) -->
      <div id="main">
        <div id="main-inside">
          <!-- primary content -->
          <div id="primary-content">
            <?php if (!empty($breadcrumb)): ?><div class="drupal-breadcrumb"><?php print $breadcrumb; ?></div><?php endif; ?>
            <?php if (!empty($title) && empty($node)): ?><h2 class="topTitle"><?php print $title; ?></h2><?php endif; ?>
            <?php if (!empty($tabs)): ?><div class="tabbed-content post-tabs"><div class="tabs-wrap"><?php print $tabs; ?></div><div class="clear"></div></div><?php endif; ?>
            <?php if (!empty($messages)): print $messages; endif; ?>
	        <?php if (!empty($help)): print $help; endif; ?>
            <?php if ($is_front && !empty($mission)): ?>
              <div class="page-navigation">
                <div class="alignleft"><a><?php print $mission; ?></a></div>
                <div class="alignright"></div>
                <div class="clear-block"></div>
              </div>
            <?php endif; ?>
            <?php print $content; ?>  
          </div>
          <!-- /primary content -->

    
          <div id="sidebar">
            <?php if ($right): ?>
              <ul class="sidebar-blocks">
                <?php print $right; ?>
              </ul>
            <?php endif; ?>
          </div>

          <div class="clear"> </div>

        </div>
      </div>
      <!-- /main content -->

 <!-- foooter -->
  <div id="footer">
    <!-- blocks + slider -->
    <div id="footer-blocks" class="page-content ">

    <!-- block container -->

	<div class="slide-container">
		<?php if ($footer): ?>
		  <ul class="slides">
		    <li class="slide slide-1 page-content">
              <div class="slide-content">
                <ul class="blocks widgetcount-3">
		          <?php print $footer; ?>
		        </ul>
              </div>
            </li>
          </ul>
        <?php endif; ?>
        <div class="clear"></div>
	  </div>
    <!-- /block container -->
   </div>
   <!-- /blocks + slider -->
 
   <div class="page-content">
     <div id="copyright">
       <?php print $footer_msg; ?> | <?php print $footer_message ?><br />
       Mystique theme by <a href="http://digitalnature.ro">digitalnature</a> ported by top <a href="http://topdrupalthemes.net">drupal themes</a> | <a href="http://www.newhostgatorcoupon.com">hostgator coupon</a>
       <br /> <?php print $feed_icons ?> <a id="goTop" href="#" class="js-link">TOP</a>     
     </div>
   </div>
 </div>
 <!-- /footer -->
 </div>
</div>
<!-- /shadow -->
</div>
<?php print $closure; ?>
</body>
</html>