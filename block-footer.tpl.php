<?php
?>
<!-- block -->

                <li class="block block-<?php print $block->module ?>" id="block-<?php print $block->module .'-'. $block->delta; ?>">
                  <div class="block-content">
                    <?php if ($block->subject): ?>
                      <h4 class="title"><?php print $block->subject ?></h4>
                    <?php endif ?>
                    <div>
                      <?php print $block->content ?>
                    </div>
                  </div>
                </li>

          <!-- /block -->