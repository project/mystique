<?php
?>
 <li class="block block-<?php print $block->module ?>" id="block-<?php print $block->module .'-'. $block->delta; ?>">
   <div class="block-widget_archive">
     <?php if ($block->subject): ?>
       <h3 class="title"><span><?php print $block->subject ?></span></h3>
     <?php endif ?>
     <div class="block-div"></div><div class="block-div-arrow"></div>	
     <?php print $block->content ?>
     </div>
 </li>