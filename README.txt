$Id 

Project page:
http://digitalnature.ro/projects/mystique

Licensed under GPL
http://www.opensource.org/licenses/gpl-license.php


CREDITS:
- digitalnature - http://digitalnature.ro (design and coding)
- Dkret3 theme by Joern Kretzschmar - http://diekretzschmars.de
- Sandbox theme - http://www.plaintxt.org/themes/sandbox
- Tarski theme - http://tarskitheme.com
- jQuery - http://jquery.com
- jQuery Flickr plug-in by Daniel MacDonald - www.projectatomic.com
- jQuery Twitter by Damien du Toit - http://coda.co.za/blog/2008/10/26/jquery-plugin-for-twitter
- loopedSlider by Nathan Searles - http://nathansearles.com/loopedslider
- clearfield by Stijn Van Minnebruggen - http://www.donotfold.be
- Fancybox by Janis Skarnelis - http://fancybox.net
- Recent comments by George Notaras - http://www.g-loaded.eu/2006/01/15/simple-recent-comments-wordpress-plugin/
- Smashing Magazine - http://smashingmagazine.com
- Wordpress - http://wordpress.org


REQUIREMENTS:
- Wordpress or Wordpress MU, 2.8+ required
- PHP: allow url fopen enabled (for TinyURL links)

TO DO: (a list of things to remember I need to do in the future)
- add hide delay to shareThis
- add yahoo link to shareThis
- better IE6 support
- add more color schemes
- use jquery for image captions
- add validation to comment form
- more options on the flickr widget
- add more shortcodes (adsense)
- add Facebook connect, twitter and OpenID login to comment box
- featured content: check if images from posts are links and show the link source image in the lightbox

CHANGE LOG:

7, 10.2009: v1.08 - design tweaks
                  - added numbered page navigation (wp-pagenavi has priority, if active)
                  - theme options changes (blue color scheme, lightbox on/off...)
                  - some bugfixes and improvements

3, 10.2009: First release (1.0)
