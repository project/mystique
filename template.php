<?php

function mystique_preprocess_page(&$vars) {
  $vars['footer_msg'] = ' &copy; ' . $vars['site_name'] . ' ' . date('Y');
  $vars['search_box'] = str_replace(t('Search this site: '), '', $vars['search_box']);
  
  $vars['p_links'] = '';
  if(!empty($vars['primary_links'])) {

    foreach ($vars['primary_links'] as $link) {
      $link_current = '';
      $attributes = 'class="page_item"';
      
      $href_attributes = 'class="fadeThis"';
      $href = url($link['href']);
      
      if ($link['href'] == '<front>') {
        $attributes = 'id="nav-home"';
        
        if (drupal_is_front_page())
        $attributes .= ' class="current_page_item"';
        
        $href_attributes = 'class="home fadeThis"';
        
      }

      if($link['href'] == $_GET['q']) {
        $attributes = 'class="current_page_item fadeThis"';
      }
      $vars['p_links'] .= '<li ' . $attributes . '><a ' . $href_attributes . ' href="' . $href . '" > <span class="title">' . $link['title'] . '</span><span class="pointer"></span></a></li>';
    }
  }
}
  
function mystique_preprocess_node(&$vars) {
  $vars['postdate'] = format_date($vars['node']->created, 'custom', 'd F Y');
  $vars['author'] = theme('username', $vars['node']);
  $vars['posted_by'] = t('By') . ' ' . $vars['author'] . ' ' . t('on') . ' ' . $vars['postdate'];
  
  
}

function mystique_preprocess_comment_wrapper(&$vars) {
  $node = $vars['node'];
  $vars['header'] = t('<strong>!count comments</strong> on %title', array('!count' => $node->comment_count, '%title' => $node->title));
}

function mystique_preprocess_comment(&$vars) {
  $vars['classes'] = array('comment');
  if ($vars['zebra'] == 'even') {
	$vars['classes'][] = 'alt';
  }
  $vars['classes'] = implode(' ', $vars['classes']);
}

